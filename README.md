# IIHE_picture_soft


## Installation
sudo yum install gvfs-gphoto2
sudo yum install snapd
sudo systemctl enable --now snapd.socket
sudo ln -s /var/lib/snapd/snap /snap
sudo snap install gphoto2
sudo install v4l2loopback-dkms


sudo dnf install --nogpgcheck https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %rhel).noarch.rpm -y
sudo dnf install --nogpgcheck https://mirrors.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-$(rpm -E %rhel).noarch.rpm -y
sudo dnf install ffmpeg ffmpeg-devel


sudo pip install piwigo
sudo pip install PyQt5
sudo pip install -U numpy
sudo pip install -U opencv-python



mkdir storage
touch storage/database.csv

reboot

source setup.sh
python3 main.py


### For Ubuntu : 
sudo apt-get install gphoto2 v4l2loopback-utils v4l2loopback-dkms ffmpeg build-essential libelf-dev linux-headers-$(uname -r) unzip vlc 
